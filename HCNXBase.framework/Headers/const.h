//
//  const.h
//  HCNXBase FrameWork
//
//  Created by Guillaume MARTINEZ on 02/12/2015.
//  Copyright © 2015 Guillaume MARTINEZ. All rights reserved.
//

#ifndef const_h
#define const_h

#define URL_BASE                        @"https://highnotifapi.hcnx.eu/api"
#define URL_BASE_RECETTE                @"http://highnotifapi-rec.hcnx.eu/api"

#define NUMERO_VERSION_1_0              @"/1.0/"
#define NUMERO_VERSION_1_2              @"/1.2/"

#define NUMERO_VERSION                  @"/1.3/"

#define URL_MANAGE_CHANNEL              @"manage_channels"
#define URL_PUSH_CHANNEL                @"get_channels"
#define URL_SUBPUSH_CHANNEL             @"subscribe_to_channels"
#define URL_UNSUBPUSH_CHANNEL           @"unsubscribe_to_channels"
#define URL_GET_HOURS                   @"get_hours"
#define URL_POST_HOURS                  @"manage_hours"
#define URL_GET_CAMPAINS                @"get_campaigns"
#define URL_GET_VERSION                 @"check_version"
#define URL_GET_MY_PUSH                 @"get_my_push"
#define URL_INIT                        @"init"
#define URL_HN_ID_FROM_FINGERPRINT      @"get_hn_ids_from_hcnxprint "

//#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

typedef NS_ENUM(NSInteger, HCNXEnvironment) {
    /**
     *  App has been downloaded from the AppStore
     */
    HCNXEnvironmentAppStore = 0,
    /**
     *  App has been downloaded from TestFlight
     */
    HCNXEnvironmentTestFlight = 1,
    /**
     *  App has been installed by some other mechanism.
     *  This could be Ad-Hoc, Enterprise, etc.
     */
    HCNXEnvironmentOther = 99
};

#define     MGSBANNERVIEW           0
#define     MGSBANNERVIEWSHADOW     1
#define     MGSFULLVIEW             2

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 667.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#endif /* const_h */
