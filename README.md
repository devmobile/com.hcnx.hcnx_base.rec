# com.hcnx.hcnx_base.rec

[![CI Status](https://img.shields.io/travis/g.martinez@highconnexion.com/com.hcnx.hcnx_base.rec.svg?style=flat)](https://travis-ci.org/g.martinez@highconnexion.com/com.hcnx.hcnx_base.rec)
[![Version](https://img.shields.io/cocoapods/v/com.hcnx.hcnx_base.rec.svg?style=flat)](https://cocoapods.org/pods/com.hcnx.hcnx_base.rec)
[![License](https://img.shields.io/cocoapods/l/com.hcnx.hcnx_base.rec.svg?style=flat)](https://cocoapods.org/pods/com.hcnx.hcnx_base.rec)
[![Platform](https://img.shields.io/cocoapods/p/com.hcnx.hcnx_base.rec.svg?style=flat)](https://cocoapods.org/pods/com.hcnx.hcnx_base.rec)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.hcnx.hcnx_base.rec is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'com.hcnx.hcnx_base.rec'
```

## Author

g.martinez@highconnexion.com, g.martinez@highconnexion.com

## License

com.hcnx.hcnx_base.rec is available under the MIT license. See the LICENSE file for more info.
